export class EmailRequest {

   public static sendEmail :any= {
        "key": "A01",
        "params": {
            "Source": "dev-do-not-reply@cftpay.com",
            "Destination": {
                "ToAddresses": [
                    "sishanglal@finxera.com"
                ]
            },
            "Message": {
                "Subject": {
                    "Data": "Test Subject"
                },
                "Body": {
                    "Text": {
                        "Data": "Test data"
                    }
                }
            }
        }
    }
    public static sendEmailError :any= {
        "key": "A01",
        "params": {
            "Source": "dev-do-not-reply@cftpay.com",
            "Destination": {
                "ToAddresses": [
                  
                ]
            },
            "Message": {
                "Subject": {
                    "Data": "Test Subject"
                },
                "Body": {
                    "Text": {
                        "Data": "Test data"
                    }
                }
            }
        }
    }
    public static sendEmailErr :any= {
        "key": "A01",
        "params": {
            
           
            "Message": {
               
            }
        }
    }
    public static sendEmailTemplate :any= {
        "key":"A01",
        "params":{
            "Source": "dev-do-not-reply@cftpay.com",
            "Template": "testTemplatelal",
            "Destination":
            {
    
              "ToAddresses": [
                "sishanglal@nivid.co"
              ]
            }
           
          },
          "replacementdata":"{ \"date\":\"17/06/2020\",\"name\":\"John Smith\",\"time\":\"9 AM\",\"dresscode\":\"Blue & White\"}"
    }
    
    public static sendEmailErrorTemplate :any= {
        "key":"A01",
        "params":{
            "Source": "dev-do-not-reply@cftpay.com",
            "Template": "",
            "Destination":
            {
    
              "ToAddresses": [
                "sishanglal@nivid.co"
              ]
            }
           
          },
          "replacementdata":"{ \"date\":\"17/06/2020\",\"name\":\"John Smith\",\"time\":\"9 AM\",\"dresscode\":\"Blue & White\"}"
    }
    public static creatUpdateTemplate:any={
        "key":"A01",
        "template":{
        "templateName":"testTemplatelal",
        "subject":"Welcome {{name}} to our company",
         "body":   "Dear {{name}},<br /><p>&nbsp; &nbsp;    We are all really excited to welcome you to our eam! s agreed, your start date is <b> {{date}}</b>.We expect you to be in our offices by<b> {{time}} </b> and our dress code is<b> {{dresscode}}</b></p> Thanks & Regards,<br />HR"
        }
    }

}
export class EmailResponse {
    public static  EmailResponse:any= {
        "ResponseMetadata": {
            "RequestId": "7ca95f49-508f-4567-b114-be97319b4ef8"
        },
        "MessageId": "01000172fef17b9d-f5221f32-117b-42c4-b22b-fe930b2edee2-000000"
    }
    public static  EmailTemplateResponse:any= {
        "ResponseMetadata": {
            "RequestId": "7ca95f49-508f-4567-b114-be97319b4ef8"
        },
        "MessageId": "01000172fef17b9d-f5221f32-117b-42c4-b22b-fe930b2edee2-000000"
    }
    public static updateTemplateResponse:any={
        "ResponseMetadata": {
            "RequestId": "7a34257b-05bd-4f97-840b-361cb6e5118b"
        }
    }
    public static createTemplateResponse:any={
        "ResponseMetadata": {
            "RequestId": "7a34257b-05bd-4f97-840b-361cb6e5118c"
        }
    }
    public static undefinedResponse=undefined
    public static sendEmailErrorResponse:any={
        "response": {
            "error": "Missing required header 'To'."
        },
        "status": 201,
        "message": "Http Exception"
    }
}