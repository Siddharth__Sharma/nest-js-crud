import { Injectable, INestApplication } from '@nestjs/common';
import { TestingModule, Test } from '@nestjs/testing';
import { SESService } from '../src/client/client/aws-ses-email/ses.service';
import { SESEmailProvider } from '../src/client/client/aws-ses-email/ses-email/email-provider/ses-email-provider';
import { mockSESEmailProvider } from './__mocks__/aws-email/mockSESEmailProvider';
import { EmailRequest, EmailResponse } from './__mocks__/aws-email/mockconstants';
import { SES } from 'aws-sdk';
import { mockSES } from './__mocks__/mocSES';
import { rejects } from 'assert';



/**
 * @author Sishanglal G P
 */

const mockSESGetObject = jest.fn();
const mockSESTemplateObject = jest.fn();
const mockCreateTemplateObject = jest.fn();
const mockUpdateTemplateObject = jest.fn();
jest.mock('aws-sdk', () => {
    return {
        SES: jest.fn(() => ({
            sendEmail: mockSESGetObject,
            sendTemplatedEmail: mockSESTemplateObject,
            createTemplate: mockCreateTemplateObject,
            updateTemplate: mockUpdateTemplateObject,
            config: { update: jest.fn() }
            , endpoint: {}
        }))
    };
});
describe('SESService', () => {
    let app: INestApplication;
    let mocksESService: SESService;
    
    /**
     * Mocking the dao service and jwt service 
     */
    const mockEmailResponse: any = {
        "ResponseMetadata": {
            "RequestId": "7ca95f49-508f-4567-b114-be97319b4ef8"
        },
        "MessageId": "01000172fef17b9d-f5221f32-117b-42c4-b22b-fe930b2edee2-000000"
    }
    const mockTemplateEmailResponse: any = {
        "ResponseMetadata": {
            "RequestId": "7ca95f49-508f-4567-b114-be97319b4ef8"
        },
        "MessageId": "01000172fef17b9d-f5221f32-117b-42c4-b22b-fe930b2edee2-000000"
    }
    const mockUpdateTemplateResponse: any = {
        "ResponseMetadata": {
            "RequestId": "7a34257b-05bd-4f97-840b-361cb6e5118b"
        }
    }
    const mockCreateTemplateResponse: any = {
        "ResponseMetadata": {
            "RequestId": "7a34257b-05bd-4f97-840b-361cb6e5118c"
        }
    }
    const mockSendEmailErrorResponse: any = {
        "response": {
            "error": "Missing required header 'To'."
        },
        "status": 201,
        "message": "Http Exception"
    }
    const mockSendEmailTemplateErrorResponse: any = {
        "response": {
            "error": "Template cannot be empty"
        },
        "status": 201,
        "message": "Http Exception"
    }
    const mockEmailCreateErrorResponse:any={
        "response": {
            "error": "Template testTemplatelal already exists for account id 862389706708"
        },
        "status": 201,
        "message": "Http Exception"
    }
    const mockUpdateTemplateErrorResponse:any={
        "response": {
            "error": "The template name must be specified."
        },
        "status": 201,
        "message": "Http Exception"
    }
    beforeAll(async () => {
        const mod: TestingModule = await Test.createTestingModule({
            providers: [SESService, SESEmailProvider],
        }).compile();
        mocksESService = mod.get<SESService>(SESService);
       

    });
    // will close the app
    afterAll(async () => {
        app.close();
    });

    describe('AWS SES Email ', () => {
        it('Send normal email Object', async () => {
            mockSESGetObject.mockImplementation((params) => {
                return {
                    promise() {
                        return Promise.resolve({ $response: { data: mockEmailResponse } })
                    }
                };
            });

            const result = await mocksESService.sendMail(EmailRequest.sendEmail.key, EmailRequest.sendEmail.params)
            const resultError = await mocksESService.sendMail(undefined, EmailRequest.sendEmail.params)

            expect(resultError).toStrictEqual(EmailResponse.undefinedResponse)

            return expect(result).toStrictEqual(EmailResponse.EmailResponse)

        });
        it('Send normal throw error email Object', async () => {
            mockSESGetObject.mockImplementation((params) => {
                return {
                    promise() {
                        return  Promise.reject({ err: { data: mockSendEmailErrorResponse } })
                    }
                };
            });

            await expect(mocksESService.sendMail(EmailRequest.sendEmailErr.key, EmailRequest.sendEmailErr.params)).resolves.toThrow()

        });
        it('Send normal email template Object', async () => {
            mockSESTemplateObject.mockImplementation((params) => {
                return {
                    promise() {
                        return Promise.resolve({ $response: { data: mockTemplateEmailResponse } })
                    }
                };
            });
            const result = await mocksESService.sendEmailTemplate(EmailRequest.sendEmailTemplate.key, EmailRequest.sendEmailTemplate.params, EmailRequest.sendEmailTemplate.replacementdata)
            const resultError = await mocksESService.sendEmailTemplate(undefined, undefined, undefined)
            const resultErrorC = await mocksESService.sendEmailTemplate(undefined, undefined, EmailRequest.sendEmailTemplate.replacementdata)

            expect(resultError).toStrictEqual(EmailResponse.undefinedResponse)
            return expect(result).toStrictEqual(EmailResponse.EmailTemplateResponse)
        });

        it('Send normal email Error template Object', async () => {
            mockSESTemplateObject.mockImplementation((params) => {
                return {
                    promise() {
                        return Promise.reject({ err: { data: mockSendEmailTemplateErrorResponse } })
                    }
                };
            });
       
            await expect(mocksESService.sendEmailTemplate(EmailRequest.sendEmailErrorTemplate.key, EmailRequest.sendEmailErrorTemplate.params, EmailRequest.sendEmailErrorTemplate.replacementdata)).resolves.toThrow()
        });
        it('Create template Object', async () => {
            mockCreateTemplateObject.mockImplementation((params) => {
                return {
                    promise() {
                        return Promise.resolve({ $response: { data: mockCreateTemplateResponse } })
                    }
                };
            });
            const result = await mocksESService.createTemplate(EmailRequest.sendEmailTemplate.key, EmailRequest.creatUpdateTemplate)
            const resultError = await mocksESService.createTemplate(undefined, undefined)
            expect(resultError).toStrictEqual(EmailResponse.undefinedResponse)

            return expect(result).toStrictEqual(EmailResponse.createTemplateResponse)
        });
        it('Create template Error Object', async () => {
            mockCreateTemplateObject.mockImplementation((params) => {
                return {
                    promise() {
                        return Promise.reject({ err: { data: mockEmailCreateErrorResponse } })
                    }
                };
            });
        await expect( mocksESService.createTemplate(EmailRequest.sendEmailTemplate.key, EmailRequest.creatUpdateTemplate)).resolves.toThrow();
          
        });
        it('Update template Object', async () => {
            mockUpdateTemplateObject.mockImplementation((params) => {
                return {
                    promise() {
                        return Promise.resolve({ $response: { data: mockUpdateTemplateResponse } })
                    }
                };
            });
            const result = await mocksESService.updateTemplate(EmailRequest.sendEmailTemplate.key, EmailRequest.creatUpdateTemplate)
            const resultError = await mocksESService.updateTemplate(undefined, undefined)
            expect(resultError).toStrictEqual(EmailResponse.undefinedResponse)
            return expect(result).toStrictEqual(EmailResponse.updateTemplateResponse)
        });
        it('Update template Error Object', async () => {
            mockUpdateTemplateObject.mockImplementation((params) => {
                return {
                    promise() {
                        return Promise.reject({ err: { data: mockUpdateTemplateErrorResponse } })
                    }
                };
            });
         await expect(mocksESService.updateTemplate(EmailRequest.sendEmailErrorTemplate.key, EmailRequest.sendEmailErrorTemplate)).resolves.toThrow();
          
        });

    });

});
