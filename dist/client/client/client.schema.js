"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.ClientSchema = new mongoose.Schema({
    id: Number,
    firstName: String,
    lastName: String,
    email: String,
    phoneNumber: String,
    city: String,
    country: String,
}, {
    versionKey: false
});
//# sourceMappingURL=client.schema.js.map