import { Client } from './client.model';
import { ClientService } from './client.service';
import { CreateClientDTO } from './create-client.dto';
export declare class ClientController {
    private clientService;
    constructor(clientService: ClientService);
    get(): Promise<Client[]>;
    create(res: any, createClientDTO: CreateClientDTO): Promise<any>;
    update(clientId: any, createClientDTO: CreateClientDTO): Promise<void>;
    delete(id: any): Promise<void>;
}
