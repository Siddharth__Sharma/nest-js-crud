"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
class CustomException extends common_1.HttpException {
    constructor(String, HttpStatus) {
        super(String, HttpStatus);
    }
}
exports.CustomException = CustomException;
//# sourceMappingURL=validation.exception.js.map