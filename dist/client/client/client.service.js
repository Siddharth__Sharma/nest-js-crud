"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const validation_exception_1 = require("./validation.exception");
const client_response_dto_1 = require("./client-response.dto");
const error_code_enum_1 = require("./error-code-enum");
let ClientService = class ClientService {
    constructor(clientSchema) {
        this.clientSchema = clientSchema;
    }
    async getAll() {
        return await this.clientSchema.find();
    }
    async create(createClientDTO) {
        const response = new client_response_dto_1.ClientResponseDTO();
        try {
            const createdClient = await new this.clientSchema(createClientDTO);
            createdClient.save();
            response.id = createdClient.id;
            response.email = createdClient.email;
        }
        catch (error) {
            throw new validation_exception_1.CustomException(error_code_enum_1.ErrorCodeEnum.UNPROCESSABLE_ENTITY, common_1.HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return response;
    }
    async update(clientId, createClientDTO) {
        let updateClient;
        try {
            const updateClient = await this.clientSchema.findByIdAndUpdate(clientId, createClientDTO, { new: true });
        }
        catch (error) {
            console.log(error);
            throw new validation_exception_1.CustomException(error_code_enum_1.ErrorCodeEnum.UNPROCESSABLE_ENTITY, common_1.HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return updateClient;
    }
    async delete(clientId) {
        let deleteClient;
        try {
            const deleteClient = await this.clientSchema.findByIdAndRemove(clientId);
        }
        catch (error) {
            throw new validation_exception_1.CustomException(error_code_enum_1.ErrorCodeEnum.UNPROCESSABLE_ENTITY, common_1.HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return deleteClient;
    }
};
ClientService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Client')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], ClientService);
exports.ClientService = ClientService;
//# sourceMappingURL=client.service.js.map