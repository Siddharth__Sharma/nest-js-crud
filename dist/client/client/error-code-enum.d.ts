export declare class ErrorCodeEnum {
    errorCode: string;
    errorMessage: string;
    static readonly UNPROCESSABLE_ENTITY: ErrorCodeEnum;
    private constructor();
}
