import { ExceptionFilter, ArgumentsHost } from "@nestjs/common";
import { CustomException } from "./validation.exception";
export declare class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: CustomException, host: ArgumentsHost): void;
}
