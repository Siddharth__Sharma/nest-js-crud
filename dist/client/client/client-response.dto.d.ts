export declare class ClientResponseDTO {
    id: Number;
    firstName: String;
    lastName: String;
    email: String;
    phoneNumber: String;
    city: String;
    country: String;
}
