import { Document } from "mongoose";
export interface Client extends Document {
    id: Number;
    firstName: String;
    lastName: String;
    email: String;
    phoneNumber: String;
    city: String;
    country: String;
}
