import { Model } from 'mongoose';
import { Client } from './client.model';
import { CreateClientDTO } from './create-client.dto';
export declare class ClientService {
    private readonly clientSchema;
    constructor(clientSchema: Model<Client>);
    getAll(): Promise<Client[]>;
    create(createClientDTO: CreateClientDTO): Promise<any>;
    update(clientId: any, createClientDTO: CreateClientDTO): Promise<Client>;
    delete(clientId: any): Promise<any>;
}
