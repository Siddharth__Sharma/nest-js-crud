import { HttpException, HttpStatus } from "@nestjs/common";

export class CustomException extends HttpException {
    constructor(String,  HttpStatus) {
      super(String, HttpStatus);
    }
  }