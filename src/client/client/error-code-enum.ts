export class ErrorCodeEnum {

    static readonly UNPROCESSABLE_ENTITY = new ErrorCodeEnum(
        'EC-BL-0001',
        'Unprocessable entity.',
      );

      private constructor(
        public errorCode: string,
        public errorMessage: string,
      ) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
      }
    
}