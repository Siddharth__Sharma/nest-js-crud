import { Injectable } from '@nestjs/common';
import { SES, AWSError } from 'aws-sdk';
import { SendEmailRequest, SendEmailResponse, SendTemplatedEmailRequest } from 'aws-sdk/clients/ses';
import { SMTPEmailProvider } from './smtp-email/email-provider/smtp-email-provider';

/**
 * @author Sruthi
 */
@Injectable()
export class SMTPService {
  constructor(private emailProvider: SMTPEmailProvider) { }

  async sendMail(sendEmailRequest:SendEmailRequest) {
    try {
      return await this.emailProvider.send(sendEmailRequest);
    }
    catch (err) {
      console.log(err)
    }
  }

  async sendEmailTemplate(sendTemplatedEmailRequest:SendTemplatedEmailRequest) {
    try {
      return await this.emailProvider.sendWithTemplate(sendTemplatedEmailRequest);
    }
    catch (err) {
      console.log(err);
    }
  }
}
