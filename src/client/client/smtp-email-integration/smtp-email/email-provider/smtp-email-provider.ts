import { IEmailProvider } from "../interface/email-provider";
import { SendEmailRequest, SendTemplatedEmailRequest } from "aws-sdk/clients/ses";
import { Smtp } from "../settings/smtp";
import { Injectable } from "@nestjs/common";
/**
 * @author Sruthi
 */
@Injectable()
export class SMTPEmailProvider implements IEmailProvider {


    async send(sendEmailRequest: SendEmailRequest) {
        try {
            return await Smtp.getInstance().sendEmail(sendEmailRequest)
        }
        catch (err) {
            console.log(err)
        }
    }
    async sendWithTemplate(sendTemplatedEmailRequest: SendTemplatedEmailRequest) {
        try {
            return await Smtp.getInstance().SendTemplatedEmail(sendTemplatedEmailRequest)
        } catch (err) {
            console.log(err)
        }
    }
   
}