import { SES, AWSError } from 'aws-sdk';
import { SendEmailRequest, SendEmailResponse, SendTemplatedEmailRequest, CreateTemplateResponse, SendTemplatedEmailResponse } from 'aws-sdk/clients/ses';
import { SMTPEmail } from '../../config/config';

/**
 * @author Sruthi
 */
export class Smtp {
    public static instance : Smtp;
    private nodemailer = require('nodemailer');
    private handlebars = require('handlebars');
    private fs = require('fs');
    private transporter ;
    private mailConfig: any = {};
    private constructor() { }
    public static getInstance(){
        if (!this.instance) {
            this.instance = new Smtp();
        }
       
        this.instance.setSmtpCongig();
        this.instance.transporter = this.instance.nodemailer.createTransport(this.instance.mailConfig);
        return this.instance;
    }

    private setSmtpCongig() {
        this.mailConfig = {
            host: SMTPEmail.smtpConfig.HOST,
            port: 587,
            auth: {
                user: SMTPEmail.smtpConfig.ACCESS_KEY_ID,
                pass: SMTPEmail.smtpConfig.SECRET_ACCESS_KEY,
            }
        };
    }
    public async sendEmail(params: SendEmailRequest) {
        try {
            
            let mailOptions = {
                from: params.Source,
                to: params.Destination.ToAddresses[0],
                subject: params.Message.Subject.Data,
                text: params.Message.Body.Text.Data,
            };
            this.transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return error;
                }else{
                    console.log(info);
                    return info;
                   
                }
            });
        }
        catch (err) {
            console.log(err)
        }
    }
    public async SendTemplatedEmail(params: SendTemplatedEmailRequest) {
        let _this=this;
        try {
            this.fs.readFile(params.Template, { encoding: 'utf-8' }, function (err, html) {
                if (err) {
                    console.log(err);
                } else {
                    let template = _this.handlebars.compile(html);
                    let replacements = JSON.parse(params.TemplateData);
                    let htmlToSend = template(replacements);
                    let mailOptions = {
                        from: params.Source,
                        to: params.Destination.ToAddresses[0],
                        html: htmlToSend
                    };
                    _this.transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            return error;
                        } else {
                            console.log(info);
                            return info;
                        }
                    });
                }
            });
        }
        catch (err) {
            console.log(err)
        }
    }

}
