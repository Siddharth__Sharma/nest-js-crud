import { SendEmailRequest,SendTemplatedEmailRequest } from 'aws-sdk/clients/ses';
/**
 * @author Sruthi
 */
export interface IEmailProvider
{
    send(sendEmailRequest:SendEmailRequest);
    sendWithTemplate(sendTemplatedEmailRequest:SendTemplatedEmailRequest);
}