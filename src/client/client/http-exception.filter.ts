import { ExceptionFilter, HttpException, ArgumentsHost, Catch } from "@nestjs/common";
import { Request, Response } from 'express';
import { CustomException } from "./validation.exception";

@Catch(CustomException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: CustomException, host: ArgumentsHost) {
      console.log(" throwing from HttpExceptionFilter");
      const ctx = host.switchToHttp();
      const response = ctx.getResponse<Response>();
      const request = ctx.getRequest<Request>();
      const status = exception.getStatus();
  
      response
        .status(status)
        .json({
          errorCode: status,
          errorMessage: exception.message
        });

    }
  }