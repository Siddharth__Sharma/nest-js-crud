import { Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Client } from './client.model';
import { CreateClientDTO } from './create-client.dto';
import { CustomException } from './validation.exception';
import { ClientResponseDTO } from './client-response.dto';
import { ErrorCodeEnum } from './error-code-enum';

@Injectable()
export class ClientService {
    constructor(@InjectModel('Client') private readonly clientSchema: Model<Client>) {}
    

    /**
     * GET CLIENTS API
     * 
     */
    async getAll(){
        return await this.clientSchema.find();
    }

    
   /**
    * CREATE CLIENT API
    *
    * contains create client response DTO. 
    * Generic response DTO is not used (Pending)
    * CustomException is class to handle the exception thrown by the api
    * currently exception handled is for unprocessable entity only.
    * ErrorCodeEnum contains the error code and the error message which will be the response
    * @param createClientDTO 
    */
    async create(createClientDTO: CreateClientDTO): Promise<any> {
        const response = new ClientResponseDTO();
        try{
        const createdClient = await new this.clientSchema(createClientDTO);
        createdClient.save();
        response.id = createdClient.id;
        response.email = createdClient.email;
        }
        catch(error){
           throw new CustomException(ErrorCodeEnum.UNPROCESSABLE_ENTITY, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return response;
      }

      /**
       * UPDATE CLIENT API
       * throw exception if unable to update, currently exception thrown as unprocessable entity only.
       * @param clientId 
       * @param createClientDTO 
       */
      async update(clientId, createClientDTO: CreateClientDTO): Promise<Client>{
          let updateClient;
          try{
          const updateClient = await this.clientSchema.findByIdAndUpdate(clientId, createClientDTO, {new:true});
          }
          catch(error){
            console.log(error);
            throw new CustomException(ErrorCodeEnum.UNPROCESSABLE_ENTITY, HttpStatus.UNPROCESSABLE_ENTITY);
         }
          return updateClient;
      }

      /**
       * DELETE CLIENT API
       * throw exception if unable to update, currently exception thrown as unprocessable entity only.
       * @param clientId
       */
      async delete(clientId) : Promise<any>{
          let deleteClient;
          try{
          const deleteClient = await this.clientSchema.findByIdAndRemove(clientId);
          }
          catch(error){
            throw new CustomException(ErrorCodeEnum.UNPROCESSABLE_ENTITY, HttpStatus.UNPROCESSABLE_ENTITY);
         }
          return deleteClient;
      }
}
