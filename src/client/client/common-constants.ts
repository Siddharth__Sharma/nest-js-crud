/**
 * class for common constants
 */
export class CommonConstants {

    public static TEMPLATE_VAR_BEGINING = "{{";
    public static TEMPLATE_VAR_ENDING = "}}";
}