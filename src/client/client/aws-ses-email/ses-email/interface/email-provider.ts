import { SendEmailRequest,SendTemplatedEmailRequest } from 'aws-sdk/clients/ses';
/**
 * @author Sishanglal
 */
export interface IEmailProvider
{
    send(sendEmailRequest:SendEmailRequest,key);
    sendWithTemplate(sendTemplatedEmailRequest:SendTemplatedEmailRequest,key);
    createTemplate(key,templateName, subject, body);
    updateTemplate(key,templateName, subject, body)
}


export class TemplateCreate {

    templateName: String;
    subject: String;
    body: any
}