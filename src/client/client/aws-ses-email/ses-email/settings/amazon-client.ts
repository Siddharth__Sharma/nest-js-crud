import { SES, AWSError } from 'aws-sdk';
import { SendEmailRequest, SendEmailResponse, SendTemplatedEmailRequest, CreateTemplateResponse, SendTemplatedEmailResponse } from 'aws-sdk/clients/ses';
import { SesEmail } from '../../config/config';
import { HttpException } from '@nestjs/common';
/**
 * @author Sishanglal
 */
export class AmazonClient {
    public static instance: AmazonClient;

    private ses: SES;
    private static mapInstance = new Map();
    private static count = 0;
    private constructor() { }
    /**
     * 
     * @param key 
     * Creating  instatnce for each subscribers
     */
    public static getInstance(key): AmazonClient {

        if (key) {
            if (this.mapInstance.has(key)) {
                AmazonClient.instance = this.mapInstance.get(key);
            }
            else {
                AmazonClient.instance = new AmazonClient();
                this.mapInstance.set(key, AmazonClient.instance);
                this.instance.ses = new SES();
                this.instance.setSesCongig(this.instance.ses)
            }

            return AmazonClient.instance;
        }
        else {
            throw new HttpException("Key not provided", 500);
        }
    }
    /**
     * 
     * @param ses 
     * Setting the ses configurations
     */
    private setSesCongig(ses: SES) {
        ses.config.update({ region: SesEmail.sesConfig.REGION, accessKeyId: SesEmail.sesConfig.ACCESS_KEY_ID, secretAccessKey: SesEmail.sesConfig.SECRET_ACCESS_KEY })
        ses.endpoint.hostname = SesEmail.sesConfig.HOST_NAME;//"email.us-east-1.amazonaws.com";
        ses.endpoint.host = SesEmail.sesConfig.HOST//"email.us-east-1.amazonaws.com"


    }
    /**
     * 
     * @param params 
     * senEmail for sending normal emails
     */
    public async sendEmail(params: SendEmailRequest) {
        try {
            let result: any = {};
            await this.ses.sendEmail(params).promise().then(x => { result = x.$response.data }, err => {
                result = { error: err.message, code: 201 }
            })
            if (result.code) {
                return new HttpException({ error: result.error }, 201)
            }

            return result
        }
        catch (err) {
            console.log(err)
        }
    }
    /**
     * 
     * @param params 
     * SendTemplatedEmail -> Will send email with template
     * dynamic value will replace with the value you provided in params
     */
    public async SendTemplatedEmail(params: SendTemplatedEmailRequest) {
        try {
            let result: any = {};
            await this.ses.sendTemplatedEmail(params).promise().then(x => { result = x.$response.data }, err => {
                result = { error: err.message, code: 201 }
            })
            if (result.code) {
                return new HttpException({ error: result.error }, 201)
            }
            return result
        }
        catch (err) {
            console.log(err)
        }
    }
    /**
     * 
     * @param templateName 
     * @param subject 
     * @param body 
     * createTemplate will create a template in AWS server
     */
    public async createTemplate(templateName, subject, body) {
        try {
            let template = {
                "Template": {
                    "TemplateName": templateName,
                    "SubjectPart": subject,
                    "TextPart": "",
                    "HtmlPart": `<!doctype html>
                    <html>
                        <head>
                        <meta charset=\"utf-8\">
                        </head>
                    <body>` +
                        body
                        +
                        `</body>
                    </html>`
                }
            }
            let result: any = {};
            await this.ses.createTemplate(template).promise().then(x => { result = x.$response.data }, err => {
                result = { error: err.message, code: 201 }
            })
            if (result.code) {
                return new HttpException({ error: result.error }, 201)
            }
            return result
        }
        catch (err) {
            console.log(err)
        }

    }
    /**
     * 
     * @param templateName 
     * @param subject 
     * @param body 
     * updateTemplate  will update the existing template in AWS server
     */
    public async updateTemplate(templateName, subject, body) {
        try {
            let template = {
                "Template": {
                    "TemplateName": templateName,
                    "SubjectPart": subject,
                    "TextPart": "",
                    "HtmlPart": `<!doctype html>
                    <html>
                        <head>
                        <meta charset=\"utf-8\">
                        </head>
                    <body>` +
                        body
                        +
                        `</body>
                    </html>`
                }
            }
            let result: any = {};
            await this.ses.updateTemplate(template).promise().then(x => { result = x.$response.data }, err => {
                result = { error: err.message, code: 201 }
            })
            if (result.code) {
                return new HttpException({ error: result.error }, 201)
            }
            return result
        }
        catch (err) {
            console.log(err)
        }

    }
}
