import { IEmailProvider } from "../interface/email-provider";
import { SendEmailRequest, SendTemplatedEmailRequest } from "aws-sdk/clients/ses";
import { AmazonClient } from "../settings/amazon-client";
import { Injectable } from "@nestjs/common";
/**
 * @author Sishanglal
 */
@Injectable()
export class SESEmailProvider implements IEmailProvider {
    client: AmazonClient;

    /**
     * 
     * @param sendEmailRequest 
     * @param key 
     * send sending a normal email
     * will create an instance if the subscriber has no instance of AmazonClient else retun the instance from memeory
     */
    async send(sendEmailRequest: SendEmailRequest, key) {
        try {
            this.client = AmazonClient.getInstance(key)
            let data = await this.client.sendEmail(sendEmailRequest);
            return data
        }
        catch (err) {
            console.log(err)
        }
    }
    /**
     * 
     * @param sendTemplatedEmailRequest 
     * @param key 
     * sendWithTemplate  for ssending the template email
     * will create an instance if the subscriber has no instance of AmazonClient else retun the instance from memeory
     */
    async sendWithTemplate(sendTemplatedEmailRequest: SendTemplatedEmailRequest, key) {
        try {
            this.client = AmazonClient.getInstance(key)
            return await this.client.SendTemplatedEmail(sendTemplatedEmailRequest)
        } catch (err) {
            console.log(err)
        }
    }
    /**
     * 
     * @param key 
     * @param templateName 
     * @param subject 
     * @param body 
     * createTemplate will create a new template
     * will create an instance if the subscriber has no instance of AmazonClient else retun the instance from memeory
     */
    async createTemplate(key, templateName, subject, body) {
        try {
            this.client = AmazonClient.getInstance(key)
            return await this.client.createTemplate(templateName, subject, body);

        } catch (err) {
            console.log(err)
        }
    }
    /**
     * 
     * @param key 
     * @param templateName 
     * @param subject 
     * @param body 
     * updateTemplate will update the existing template
     *  will create an instance if the subscriber has no instance of AmazonClient else retun the instance from memeory
     */
    async updateTemplate(key: any, templateName: any, subject: any, body: any) {
        try {
            this.client = AmazonClient.getInstance(key)

            return await this.client.updateTemplate(templateName, subject, body);

        } catch (err) {
            console.log(err)
        }
    }
}