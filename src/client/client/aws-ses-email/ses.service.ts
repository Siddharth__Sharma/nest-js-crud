import { Injectable } from '@nestjs/common';
import { SES, AWSError } from 'aws-sdk';
import { SendEmailRequest, SendEmailResponse, SendTemplatedEmailRequest } from 'aws-sdk/clients/ses';
import { SESEmailProvider } from './ses-email/email-provider/ses-email-provider';
import { TemplateCreate } from './ses-email/interface/email-provider';
/**
 * @author Sishanglal
 */
@Injectable()
export class SESService {

  constructor(private emailProvider: SESEmailProvider) { }
  /**
   * 
   * @param key 
   * @param params 
   * sendMail sending a normal email
   */
  async sendMail(key, params: SendEmailRequest) {
    try {
      let data = await this.emailProvider.send(params, key);
      return data
    }
    catch (err) {
      console.log(err)
    }

  }
  /**
   * 
   * @param key 
   * @param params 
   * @param replacementData 
   * sendEmailTemplate for sending template email
   */
  async sendEmailTemplate(key, params: SendTemplatedEmailRequest, replacementData: any) {
    try {
      params.TemplateData = replacementData;
      return await this.emailProvider.sendWithTemplate(params, key);

    }
    catch (err) {
      console.log(err);
    }
  }
  /**
   * 
   * @param key 
   * @param params 
   * createTemplate will create a new template
   */
  async createTemplate(key, params: TemplateCreate) {
    try {

      return await this.emailProvider.createTemplate(key, params.templateName, params.subject, params.body);

    }
    catch (err) {
      console.log(err);
    }
  }
  /**
   * 
   * @param key 
   * @param params 
   * updateTemplate will update the existing template
   */
  async updateTemplate(key, params: TemplateCreate) {
    try {

      return await this.emailProvider.updateTemplate(key, params.templateName, params.subject, params.body);

    }
    catch (err) {
      console.log(err);
    }
  }
}
