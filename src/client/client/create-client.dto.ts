import { IsNotEmpty, IsEmail, IsNumberString } from "class-validator";

export class CreateClientDTO {
    @IsNumberString()
    id: Number;
    @IsNotEmpty() firstName: String;
    lastName: String;
    @IsEmail()
    email: String;
    phoneNumber: String;
    city: String;
    country: String;
  }
