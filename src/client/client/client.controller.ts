import { Body, Controller, Post, Res } from '@nestjs/common';
import { SendEmailRequest, SendTemplatedEmailRequest } from 'aws-sdk/clients/ses';
import * as fs from 'fs';
import { SESService } from './aws-ses-email/ses.service';
import { AWSTest } from './file-service/aws/aws-test';
import { SFTPTest } from './file-service/sftp-integration/sftp-test';
import { SMTPService } from './smtp-email-integration/smtp.service';
import { TwilioTest } from './twilio-integration/twilio-test';
import { IFileRequest, ICopyFileRequest } from './file-service/file-request';
import { TemplateCreate } from './aws-ses-email/ses-email/interface/email-provider';
import { SendSMSDto } from './twilio-integration/send-sms.dto';

/**
 * @author Supriya
 */
@Controller('test')
export class ClientController {

    constructor(private twilioTest: TwilioTest, private sftpTest: SFTPTest, private awsService: AWSTest, private readonly sesAwsService: SESService, private readonly smtpService: SMTPService) { }


    @Post('sms')
    async testTwilioSMS(@Res() res, @Body() sendSMSDTO: SendSMSDto): Promise<any> {
        try {
            const data = await this.twilioTest.sendSms(sendSMSDTO);
            console.log("inside controller data .. ", data);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
            return err;
        }
    }

    @Post('downloadSftp')
    async testDownloadSFTP(@Res() res, @Body() fileRequest: IFileRequest) {
        try {
            const data = await this.sftpTest.sftpGet(fileRequest);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
            return err;
        }
    }

    @Post('uploadSftp')
    async testUploadStreamSFTP(@Res() res, @Body() fileRequest: IFileRequest) {
        try {
            const data = await this.sftpTest.sftpPut(fileRequest);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
            return err;
        }
    }

    @Post('copySftp')
    async testCopySFTP(@Res() res, @Body() copyFileRequest: ICopyFileRequest) {
        try {
            const data = await this.sftpTest.sftpCopy(copyFileRequest);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
            return err;
        }
    }

    @Post('deleteSftp')
    async testDeleteSFTP(@Res() res, @Body() fileRequest: IFileRequest) {
        try {
            const data = await this.sftpTest.sftpDelete(fileRequest);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
            return err;
        }
    }

    /**
     * AWS integration
     */
    /** contain name, localPath, remotePath */
    @Post('s3Download')
    async s3download(@Res() res, @Body() fileRequest: IFileRequest) {
        try {
            const data = await this.awsService.s3Get(fileRequest);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
        }
    }

    /** contain name, localPath, remotePath */
    @Post('s3Delete')
    async s3delete(@Res() res, @Body() fileRequest: IFileRequest) {
        try {
            const data = await this.awsService.s3Delete(fileRequest);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
        }
    }

    /** contain ICopyFileRequest */
    @Post('s3Copy')
    async s3Copy(@Res() res, @Body() fileRequest: ICopyFileRequest) {
        try {
            const data = await this.awsService.s3Copy(fileRequest);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
        }
    }

    /** contain name, localPath, remotePath */
    @Post('s3Upload')
    async s3upload(@Res() res, @Body() fileRequest: IFileRequest) {
        try {
            const data = await this.awsService.s3PutSync(fileRequest);
            return res.json(data);
        }
        catch (err) {
            console.log(err);
        }
    }

    /** put and buffer put sync */
    @Post('s3UploadStream')
    async s3uploadStream(@Res() res, @Body() fileRequest: IFileRequest) {
        try {
            let buffer = fs.readFileSync(fileRequest.localPath);
            fileRequest.buffer = buffer;
            await this.awsService.s3PutSync(fileRequest);
            return res.json("Successfully Uploaded on s3");
        }
        catch (err) {
            console.log(err);
        }
    }

    /** put  callback */
    @Post('s3UploadCb')
    async s3uploadCb(@Res() res, @Body() fileRequest: IFileRequest) {
        try {
            let buffer = fs.readFileSync(fileRequest.localPath);
            fileRequest.buffer = buffer;
            await this.awsService.s3Put(fileRequest);
            return res.json("Successfully Uploaded on s3");
        }
        catch (err) {
            console.log(err);
        }
    }


    /**
     * AWS SES Integration
     */
    @Post('ses')
    async send(@Body('key') key, @Body('params') params: SendEmailRequest) {
        try {
            let data = await this.sesAwsService.sendMail(key, params);

            return data
        }
        catch (err) {
            console.log(err)
        }
    }
    @Post('sesTemplate')
    async sendWithTemplate(@Body('key') key, @Body('params') params: SendTemplatedEmailRequest, @Body('replacementdata') replacementData: any) {
        try {
            return await this.sesAwsService.sendEmailTemplate(key, params, replacementData);
        }
        catch (err) {
            console.log(err)
        }
    }
    @Post('createtemplate')
    async createTemplate(@Body('key') key, @Body('template') params: TemplateCreate) {
        try {
            return await this.sesAwsService.createTemplate(key, params);
        }
        catch (err) {
            console.log(err)
        }
    }
    @Post('updatetemplate')
    async updateTemplate(@Body('key') key, @Body('template') params: TemplateCreate) {
        try {
            let data = await this.sesAwsService.updateTemplate(key, params);
            return data
        }
        catch (err) {
            console.log(err)
        }
    }

    /**
     * SMTP Integration
     */
    @Post('smtp')
    async sendSMTP(@Body() sendEmailRequest: SendEmailRequest) {
        try {
            return await this.smtpService.sendMail(sendEmailRequest);
        }
        catch (err) {
            console.log(err)
        }
    }
    @Post('smtpTemplate')
    async sendWithTemplateSMTP(@Body() sendTemplatedEmailRequest: SendTemplatedEmailRequest) {
        try {
            return await this.smtpService.sendEmailTemplate(sendTemplatedEmailRequest);
        }
        catch (err) {
            console.log(err)
        }
    }
}


/** test apis */
/* @Get()
    async get(): Promise<Client[]> {
        return this.clientService.getAll();
    }

    @Post()
    async create(@Res() res , @Body() createClientDTO: CreateClientDTO): Promise<any>{
        const client = await this.clientService.create(createClientDTO);
        return res.json(client);
    }

    @Post(':id')
    async update(@Param('id') clientId, @Body() createClientDTO: CreateClientDTO){
        await this.clientService.update(clientId, createClientDTO);
    }

    @Delete(':id')
    async delete(@Param('id') id){
        await this.clientService.delete(id);
     }*/