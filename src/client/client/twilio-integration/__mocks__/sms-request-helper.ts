import { TwilioProperties } from "../twilio-properties";
import { SMSRequest } from "../SMSRequest";
import { SMSTemplateRequest } from "../SMSTemplateRequest";
import { SendSMSDto } from "../send-sms.dto";

export class SMSRequestHelper {

    public static SEND_SMS_REQUEST : SMSRequest = {
        fromNumber: TwilioProperties.TWILIO_FROM_NUMBER,
        toNumber: TwilioProperties.TO_NUMBER,
        smsBody: "Hello, this is the test message.",
        // params: null
    }

    public static SEND_SMS_RESPONSE = {
        body: "Hello, this is the test message.",
        numSegments: "1",
        direction: "outbound-api",
        from: "+14243484543",
        to: TwilioProperties.TO_NUMBER,
        dateUpdated: expect.anything(),
        price: null,
        errorMessage: null,
        uri: expect.anything(),
        accountSid: "AC5a440a92da2e9990824ff31960a65dbb",
        numMedia: "0",
        status: "queued",
        messagingServiceSid: null,
        sid: expect.anything(),
        dateCreated: expect.anything(),
        dateSent: null,
        errorCode: null,
        priceUnit: "USD",
        apiVersion: "2010-04-01",
        subresourceUris: {
            media: expect.anything(),
        }
    }

    public static SEND_SMS_WITHOUT_SMS_BODY_REQUEST : SMSRequest = {
        fromNumber: TwilioProperties.TWILIO_FROM_NUMBER,
        toNumber: TwilioProperties.TO_NUMBER,
        smsBody: null,
        // params: null
    }
    
    public static SEND_SMS_WITHOUT_SMS_BODY_RESPONSE = {
        body: "This is a test@message",
        numSegments: "1",
        direction: "outbound-api",
        from: "+14243484543",
        to: TwilioProperties.TO_NUMBER,
        dateUpdated: expect.anything(),
        price: null,
        errorMessage: null,
        uri: expect.anything(),
        accountSid: "AC5a440a92da2e9990824ff31960a65dbb",
        numMedia: "0",
        status: "queued",
        messagingServiceSid: null,
        sid: expect.anything(),
        dateCreated: expect.anything(),
        dateSent: null,
        errorCode: null,
        priceUnit: "USD",
        apiVersion: "2010-04-01",
        subresourceUris: {
            media: expect.anything(),
        }
    }

    public static INCORRECT_PHONE_NUMBER_REQUEST : SMSRequest = {
        fromNumber: TwilioProperties.TWILIO_FROM_NUMBER,
        toNumber: "3456",
        smsBody: "Incorrect phone number, SMS request.",
        // params: null
    }

    public static SEND_TEMPLATE_SMS_REQUEST : SMSTemplateRequest = {
        fromNumber: TwilioProperties.TWILIO_FROM_NUMBER,
        toNumber: TwilioProperties.TO_NUMBER,
        params: [
            {
                "key": "USER_NAME",
                "value": "Supriya"
            },
            {
                "key": "LAST_NAME",
                "value": "Sharma"
            }
        ],
        smsBody: "Hello {{USER_NAME}} {{LAST_NAME}}, this is the test message."
    }

    public static SEND_TEMPLATE_SMS_RESPONSE = {
        body: "Hello Supriya Sharma, this is the test message.",
        numSegments: "1",
        direction: "outbound-api",
        from: "+14243484543",
        to: TwilioProperties.TO_NUMBER,
        dateUpdated: expect.anything(),
        price: null,
        errorMessage: null,
        uri: expect.anything(),
        accountSid: "AC5a440a92da2e9990824ff31960a65dbb",
        numMedia: "0",
        status: "queued",
        messagingServiceSid: null,
        sid: expect.anything(),
        dateSent: null,
        dateCreated: expect.anything(),
        errorCode: null,
        priceUnit: "USD",
        apiVersion: expect.anything(),
        subresourceUris: {
            media: expect.anything(),
        }
    }

    public static TEMPLATE_WITHOUT_SMS_BODY_REQUEST : SMSTemplateRequest = {
        fromNumber: TwilioProperties.TWILIO_FROM_NUMBER,
        toNumber: TwilioProperties.TO_NUMBER,
        params: [
            {
                "key": "USER_NAME",
                "value": "Supriya"
            },
            {
                "key": "LAST_NAME",
                "value": "Sharma"
            }
        ],
        smsBody: null
    }

    public static TEMPLATE_WITHOUT_SMS_BODY_RESPONSE = {
        body: "Hello Supriya Sharma, this is twilio test",
        numSegments: "1",
        direction: "outbound-api",
        from: "+14243484543",
        to: TwilioProperties.TO_NUMBER,
        dateUpdated: expect.anything(),
        price: null,
        errorMessage: null,
        uri: expect.anything(),
        accountSid: "AC5a440a92da2e9990824ff31960a65dbb",
        numMedia: "0",
        status: "queued",
        messagingServiceSid: null,
        sid: expect.anything(),
        dateSent: null,
        dateCreated: expect.anything(),
        errorCode: null,
        priceUnit: "USD",
        apiVersion: expect.anything(),
        subresourceUris: {
            media: expect.anything(),
        }
    }

    public static TEMPLATE_INCORRECT_PHONE_REQUEST : SMSTemplateRequest = {
        fromNumber: TwilioProperties.TWILIO_FROM_NUMBER,
        toNumber: "345",
        params: [
            {
                "key": "USER_NAME",
                "value": "Supriya"
            },
            {
                "key": "LAST_NAME",
                "value": "Sharma"
            }
        ],
        smsBody: "Incorrect phone number, this is the test message."
    }

    public static TEMPLATE_VARIABLE_NOT_PRESENT_REQUEST : SMSTemplateRequest = {
        fromNumber: TwilioProperties.TWILIO_FROM_NUMBER,
        toNumber: TwilioProperties.TO_NUMBER,
        params: [
            {
                "key": "USER_NAME",
                "value": "Supriya"
            },
            {
                "key": "MIDDLE_NAME",
                "value": "Sharma"
            }
        ],
        smsBody: "Hello {{USER_NAME}} {{LAST_NAME}}, this is the test message."
    }

    public static TEMPLATE_VARIABLE_NOT_PRESENT_RESPONSE = {
        body: "Hello Supriya {{LAST_NAME}}, this is the test message.",
        numSegments: "1",
        direction: "outbound-api",
        from: "+14243484543",
        to: TwilioProperties.TO_NUMBER,
        dateUpdated: expect.anything(),
        price: null,
        errorMessage: null,
        uri: expect.anything(),
        accountSid: "AC5a440a92da2e9990824ff31960a65dbb",
        numMedia: "0",
        status: "queued",
        messagingServiceSid: null,
        sid: expect.anything(),
        dateSent: null,
        dateCreated: expect.anything(),
        errorCode: null,
        priceUnit: "USD",
        apiVersion: expect.anything(),
        subresourceUris: {
            media: expect.anything(),
        }
    }


}