import { Test, TestingModule } from '@nestjs/testing';
import { TwilioTest } from './twilio-test';
import { TwilioSMSProvider } from './twilio-sms-provider';
import { ISMSProvider } from './isms-provider';
import { SMSRequestHelper } from './__mocks__/sms-request-helper';
import { SMSRequest } from './SMSRequest';
import { SMSTemplateRequest } from './SMSTemplateRequest';



describe('TwilioService', () => {
    let smsProvider: ISMSProvider;
    beforeAll(async () => {
        smsProvider = new TwilioSMSProvider("test");
    });




    describe('test twilio sms', () => {
      it('should send simple text sms', async () => {
            jest.spyOn<any, any>(smsProvider, "sendTwilioSMS").mockResolvedValue(SMSRequestHelper.SEND_SMS_RESPONSE);
            const result = await smsProvider.send(SMSRequestHelper.SEND_SMS_REQUEST);
            return expect(result).toMatchObject(SMSRequestHelper.SEND_SMS_RESPONSE);
        });

        it('should throw error for simple text sms', async () => {
            try{
            const result = await smsProvider.send(SMSRequestHelper.INCORRECT_PHONE_NUMBER_REQUEST); 
            }
            catch(e){
                return expect(e).toStrictEqual(e);
            }
        });


        it('should send template text sms', async () => {
            jest.spyOn<any, any>(smsProvider, "sendTwilioSMS").mockResolvedValue(SMSRequestHelper.SEND_TEMPLATE_SMS_RESPONSE);
            const result = await smsProvider.sendWithTemplate(SMSRequestHelper.SEND_TEMPLATE_SMS_REQUEST); 
            return expect(result).toMatchObject(SMSRequestHelper.SEND_TEMPLATE_SMS_RESPONSE);
        });

        it('should throw error for template text sms', async () => {
            try{
            const result = await smsProvider.sendWithTemplate(SMSRequestHelper.TEMPLATE_INCORRECT_PHONE_REQUEST); 
            }
            catch(e){
                return expect(e).toStrictEqual(e);
            }
        });

        it('should use default template text sms', async () => {
            jest.spyOn<any, any>(smsProvider, "sendTwilioSMS").mockResolvedValue(SMSRequestHelper.TEMPLATE_WITHOUT_SMS_BODY_RESPONSE);
            const result = await smsProvider.sendWithTemplate(SMSRequestHelper.TEMPLATE_WITHOUT_SMS_BODY_REQUEST); 
            return expect(result).toMatchObject(SMSRequestHelper.TEMPLATE_WITHOUT_SMS_BODY_RESPONSE);
        });


        it('should use default simple text sms', async () => {
            jest.spyOn<any, any>(smsProvider, "sendTwilioSMS").mockResolvedValue(SMSRequestHelper.SEND_SMS_WITHOUT_SMS_BODY_RESPONSE);
            const result = await smsProvider.send(SMSRequestHelper.SEND_SMS_WITHOUT_SMS_BODY_REQUEST);
            return expect(result).toMatchObject(SMSRequestHelper.SEND_SMS_WITHOUT_SMS_BODY_RESPONSE);
        });

        it('should say error as no variable found to be replaced in template text sms', async () => {
            jest.spyOn<any, any>(smsProvider, "sendTwilioSMS").mockResolvedValue(SMSRequestHelper.TEMPLATE_VARIABLE_NOT_PRESENT_RESPONSE);
            const result = await smsProvider.sendWithTemplate(SMSRequestHelper.TEMPLATE_VARIABLE_NOT_PRESENT_REQUEST);
            return expect(result).toMatchObject(SMSRequestHelper.TEMPLATE_VARIABLE_NOT_PRESENT_RESPONSE);
        });
    });
});
