import { TwilioProperties } from "./twilio-properties";
import { ISMSProvider } from "./isms-provider";

/**
 * This will return the instance based on key for which servcie is required
 * @author Supriya
 */
export class TwilioServiceProvider {

    private static twilioServiceMap: Map<String, any> = new Map<String, String>();


    static getInstance(key: String): ISMSProvider {
        if (!this.twilioServiceMap.get(key)) {
            this.twilioServiceMap.set(key, this.getTwilioClient(key));
        }
        return this.twilioServiceMap.get(key);
    }

    private static getTwilioClient(key: String): Promise<any> {
        if (key = "test") {
            /**
             * set the account details required for twilio integration
             */
            const accountSid = TwilioProperties.TWILIO_KEY_ID;
            const authToken = TwilioProperties.TWILIO_AUTH_TOKEN;
            const service = TwilioProperties.TWILIO;

            const client = require(service)(accountSid, authToken);

            /** after client creation set all the sms request params */
            return client;
        }
    }

}