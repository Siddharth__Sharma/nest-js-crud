import { SMSRequest } from "./SMSRequest";
import { SMSTemplateRequest } from "./SMSTemplateRequest";

/**
 * interface containing send method which will accept send sms request values
 */
export interface ISMSProvider {
    send(ismsRequest: SMSRequest);
    sendWithTemplate(smsTemplateRequest: SMSTemplateRequest);
}