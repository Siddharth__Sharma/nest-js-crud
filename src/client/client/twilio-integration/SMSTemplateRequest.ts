import { IParams } from "./IParams";


export class SMSTemplateRequest {
    smsBody: String;
    fromNumber: String;
    toNumber: String;
    params: Array<IParams>;
}