import { SMSRequest } from "./SMSRequest";
import { SMSTemplateRequest } from "./SMSTemplateRequest";
import { TwilioSMSProvider } from "./twilio-sms-provider";
import { SendSMSDto } from "./send-sms.dto";

/**
 * Test class which will initialize the service and call its methods to send SMS
 * @author Supriya
 */
export class TwilioTest {
    
    /**
     * this will provide the phone number and other request values for sending sms
     */
    sendSms(sendSMSDTO: SendSMSDto) {
        const key = "test";
        try {
            let request;
            if (sendSMSDTO.params != null) {
                request = new SMSTemplateRequest();
            }
            else {
                request = new SMSRequest();
            }
            let smsProvider = new TwilioSMSProvider(key);
            /** for static text message */
            if (request instanceof SMSRequest) {
                request.toNumber = sendSMSDTO.toNumber;
                request.smsBody = sendSMSDTO.message;
                const res = smsProvider.send(request);
                return res;
            }
            /** for text message with a template */
            if (request instanceof SMSTemplateRequest) {
                request.params = sendSMSDTO.params;
                request.toNumber = sendSMSDTO.toNumber;
                request.smsBody=sendSMSDTO.message;
                const res = smsProvider.sendWithTemplate(request);
                return res;
            }
        }
        catch (e) {
            console.log("some error occured while sending message",e);
            return e;
        }
    }
}

