import { ISMSProvider } from "./isms-provider";
import { SMSRequest } from "./SMSRequest";
import { SMSTemplateRequest } from "./SMSTemplateRequest";
import { TwilioServiceProvider } from "./twilio-service-provider";
import { TwilioProperties } from "./twilio-properties";
import { CommonConstants } from "../common-constants";

/**
 * this class will send the sms using twilio sms provider service
 * @author Supriya
 */
export class TwilioSMSProvider implements ISMSProvider {
    private key: String;
    constructor(key: String) {
        this.key = key;
    }


    /** if got SMSRequest */
    send(smsRequest: SMSRequest) {
        const twilioInstance = TwilioServiceProvider.getInstance(this.key);
        /** prepare request values to be send to twilio */
        const smsBody = smsRequest.smsBody?smsRequest.smsBody:TwilioProperties.MESSAGE_BODY_TEST;
        const fromNumber = TwilioProperties.TWILIO_FROM_NUMBER;
        const toNumber = smsRequest.toNumber.toString();
        const client = twilioInstance;

        /** this will send the SMS based on provided parameters */
        const res = this.sendTwilioSMS(smsBody, fromNumber, toNumber, client);
        return res;
    }

    /** if got SMSTemplateRequest */
    sendWithTemplate(smsTemplateRequest: SMSTemplateRequest) {
        const twilioInstance = TwilioServiceProvider.getInstance(this.key);
        /** prepare request values to be send to twilio */
        const fromNumber = TwilioProperties.TWILIO_FROM_NUMBER;
        const toNumber = smsTemplateRequest.toNumber.toString();
        const client = twilioInstance;

        
        /** get the template to send SMS */
        var template = smsTemplateRequest.smsBody?smsTemplateRequest.smsBody:TwilioProperties.SMS_TEMPLATE;
        console.log("template is >>> ", template);
        if (smsTemplateRequest.params != null) {
            for (var param of smsTemplateRequest.params) {
                /** searchVal obtained from parameter key if present in template and replace with parameter value */
                const searchVal = CommonConstants.TEMPLATE_VAR_BEGINING.concat(param.key.toString()).concat(CommonConstants.TEMPLATE_VAR_ENDING);
                if (template.search(searchVal) === -1) {
                    console.log("not found");
                }
                else {
                    var regex = new RegExp(searchVal, "g");
                    console.log("string search value >> ", searchVal);
                    template = template.replace(regex, param.value.toString());
                }
            }
            console.log("after replacing value >>> ", template);
        }

        /** this will send the SMS based on provided parameters */
        const res = this.sendTwilioSMS(template, fromNumber, toNumber, client);
        return res;
    }



    private async sendTwilioSMS(smsBody: String, fromNumber: String, toNumber: any, client: any) {
        /**
         * create twilio client to send the sms
         */
        try{
        const res = await client.messages
            .create({
                body: smsBody,
                from: fromNumber,
                to: toNumber
            });
          return res;  
        }
        catch(e){
            console.log("Some exception occured while sending sms",e);
            return e;
        }
    }
}