import { IParams } from "./IParams";

export class SendSMSDto {
    
    toNumber: String;
    message: String;
    params: Array<IParams>;
  }