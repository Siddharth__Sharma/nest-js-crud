import { IFileRequest, ICopyFileRequest } from "./file-request";
export interface IFileService{
    putSync(file:IFileRequest): Promise<any>;
    put(file: IFileRequest, cb: Function): any;
    delete(file: IFileRequest) : Promise<any>;
    get(path: any) : Promise<any>;
    copy(path: ICopyFileRequest) : void;
}
