export interface IFileRequest{
    name?: string;
    localPath?: string;
    remotePath?: string;
    base64?: string;
    buffer?: any;
}

export interface ICopyFileRequest{
    sourceName?: string;
    source?: string;
    target?: string;
    targetName?: string;
}