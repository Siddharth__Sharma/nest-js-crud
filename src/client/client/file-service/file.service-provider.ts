import { AwsServiceProvider } from "./aws/aws.service";
import { IFileService } from "./file-service";
import { FileServiceProviderEnum } from "./file-service-provider.enum";
import { SFTPProvider } from "./sftp-integration/sftp-provider";

export class FileServiceProvider{
    private static  INSTANCE = new FileServiceProvider();
    private constructor(){}

    public static getInstance(){
        return this.INSTANCE;
    }

    /**
     * 
     * @param key 
     * @param instanceKey 
     */
    public async getFileServiceProvider(key: FileServiceProviderEnum, instanceKey: string): Promise<IFileService>{
        switch(key){
            case FileServiceProviderEnum.AWS_S3:
                return await AwsServiceProvider.getInstance(instanceKey);
            case FileServiceProviderEnum.SFTP: 
                return await SFTPProvider.getInstance(instanceKey);
            default:
                throw new Error(key +" : Service not supported")
        }
    }


}


