import { IFileService } from "../file-service";
import { FileServiceProvider } from "../file.service-provider";
import { FileServiceProviderEnum } from "../file-service-provider.enum";
import { IFileRequest, ICopyFileRequest } from "../file-request";
import { SFTPRequestHelper } from "./__mocks__/mock-sftp-request-helper";
import { CommonConstants } from "../common-methods";
import * as fs from 'fs';

describe('SftpService', () => {
    let sftpService: IFileService;
    beforeAll(async () => {
        sftpService = await FileServiceProvider.getInstance().getFileServiceProvider(
            FileServiceProviderEnum.SFTP,
            'test',
        );
    });


    it('should upload the file to sftp location', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_update/test_file_spec_upload.csv",
            base64: "RG9jdW1lbnQgU2VydmljZSBJZCxEb2N1bWVudCBOYW1lLFZlbmRvcixFeHRlcm5hbCBJZCxTdGF0dXMsU3RhdHVzIFJlYXNvbixBZGRyZXNzIExpbmUxLEFkZHJlc3MgTGluZTIsQ2l0eSxTdGF0ZSxaaXAgQ29kZSxQcm9jZXNzaW5nIERhdGUsRG9jdW1lbnQgRGV0YWlscwoiNzMyMiIsLCwiQjExMzkwIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyMyIsLCwiQjExMzkxIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyNCIsLCwiQjExMzkyIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyNSIsLCwiQjExMzkzIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyNiIsLCwiQjExMzk0IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyNyIsLCwiQjExMzk1IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyOCIsLCwiQjExMzk2IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMzMCIsLCwiQjExMzk4IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyOSIsLCwiQjExMzk3IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMzMSIsLCwiQjExMzk5IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMzMyIsLCwiQjExNDAxIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMzMiIsLCwiQjExNDAwIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLA"
        }
        const result = await sftpService.putSync(request);
        return expect(result).toBe("Uploaded data stream to /firefly/PNX/document_product_update/test_file_spec_upload.csv");
    }, 30000);

    it('should throw error while uploading the file to sftp location', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_updat/test_file_spec_upload.csv",
            base64: "RG9jdW1lbnQgU2VydmljZSBJZCxEb2N1bWVudCBOYW1lLFZlbmRvcixFeHRlcm5hbCBJZCxTdGF0dXMsU3RhdHVzIFJlYXNvbixBZGRyZXNzIExpbmUxLEFkZHJlc3MgTGluZTIsQ2l0eSxTdGF0ZSxaaXAgQ29kZSxQcm9jZXNzaW5nIERhdGUsRG9jdW1lbnQgRGV0YWlscwoiNzMyMiIsLCwiQjExMzkwIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyMyIsLCwiQjExMzkxIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyNCIsLCwiQjExMzkyIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyNSIsLCwiQjExMzkzIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyNiIsLCwiQjExMzk0IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyNyIsLCwiQjExMzk1IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyOCIsLCwiQjExMzk2IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMzMCIsLCwiQjExMzk4IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMyOSIsLCwiQjExMzk3IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMzMSIsLCwiQjExMzk5IiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMzMyIsLCwiQjExNDAxIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLAoiNzMzMiIsLCwiQjExNDAwIiwiU0VOVCIsLCwsLCwsIjIwMjAvMDYvMTIiLA"
        }
        try{
        const result = await sftpService.putSync(request);
        }
        catch(e){
            return expect(e).toStrictEqual(e);
        }
        
    },30000);

    it('should throw error while uploading the file to sftp location using path', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_updat/test_file_spec_upload_new.csv",
            localPath: CommonConstants.TMP_PATH+"test_file_spec_upload.csv"
        }
        try{
        const result = await sftpService.putSync(request);
        }
        catch(e){
            return expect(e).toStrictEqual(e);
        }
        
    },30000);

    it('should upload the file to sftp location using buffer', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_update/test_file_spec_upload.csv",
            localPath: CommonConstants.TMP_PATH+"test_file_spec_upload.csv"
        }
        await sftpService.get(request);
        const putRequest: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_update/test_file_spec_upload_new.csv",
            localPath: CommonConstants.TMP_PATH+"test_file_spec_upload.csv"
        }
        putRequest.buffer = fs.createReadStream(putRequest.localPath);
        const result = await sftpService.putSync(putRequest);
        return expect(result).toStrictEqual(expect.stringMatching("Uploaded data stream to /firefly/PNX/document_product_update/test_file_spec_upload_new.csv"));
    }, 30000);

    it('should download the file from sftp location', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_update/test_file_spec_upload.csv"
        }
        const result = await sftpService.get(request);
        return expect(Buffer.from(result).toJSON()).toMatchObject(SFTPRequestHelper.SFTP_DOWNLOAD_RESULT);
    }, 30000);

    it('should throw error while downloading the file from sftp location', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_updat/test_file_spec_upload.csv"
        }
        try{
        const result = await sftpService.get(request);
        }
        catch(e){
            return expect(e).toStrictEqual(e);
        }
        
    }, 30000);


    it('should throw error while downloading the file using paths', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_updat/test_file_spec_upload.csv",
            localPath: CommonConstants.TMP_PATH+"test_file_spec_upload.csv"
        }
        try{
        const result=await sftpService.get(request);
        }
        catch(e){
            return expect(e).toStrictEqual(e);
        }
    },30000);

    it('should copy the file from one directory location to another on sftp', async () => {
        const copyRequest: ICopyFileRequest = {
            source: "/firefly/PNX/document_product_update/test_file_spec_upload.csv",
            target: "/firefly/PNX/document_instruction/test_file_spec_upload.csv"
        }
        const result = await sftpService.copy(copyRequest);
        return expect(result).toStrictEqual(expect.stringMatching("successfully uploaded to"));
    }, 30000);

    it('should delete the file from uploaded sftp location', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_update/test_file_spec_upload.csv"
        }
        const result = await sftpService.delete(request);
        return expect(result).toBe("Successfully deleted file");
    }, 30000);

    it('should throw error while deleting the file from uploaded sftp location', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_updat/test_file_spec_upload.csv"
        }
        try{
        const result = await sftpService.delete(request);
        }
        catch(e){
            return expect(e).toStrictEqual(e);
        }
    }, 30000);

    it('should delete the file from copied sftp location', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_instruction/test_file_spec_upload.csv"
        }
        const result = await sftpService.delete(request);
        return expect(result).toBe("Successfully deleted file");
    }, 30000);

    it('should delete the file from uploaded sftp location using buffer', async () => {
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_update/test_file_spec_upload_new.csv",
        }
        const result = await sftpService.delete(request);
        return expect(result).toBe("Successfully deleted file");
    }, 30000);

    it('should throw error if we are calling put method', ()=>{
        const request: IFileRequest = {
            remotePath: "/firefly/PNX/document_product_update/test_file_spec_upload_new.csv",
        }
        try{
        const result = sftpService.put(request,()=>{});
        }
        catch(e){
            return expect(e).toStrictEqual(e);
        }
    })

});