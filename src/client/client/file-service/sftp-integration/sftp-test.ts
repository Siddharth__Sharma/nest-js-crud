import { ICopyFileRequest, IFileRequest } from "../file-request";
import { FileServiceProviderEnum } from "../file-service-provider.enum";
import { FileServiceProvider } from "../file.service-provider";

/**
 * Test class which will initialize the service and call the methods 
 * of sftp for upload, download, copy, delete
 * @author Supriya
 */
export class SFTPTest {
    async sftpGet(fileRequest: IFileRequest) {
        try {
            const key = FileServiceProviderEnum.SFTP;
            const instanceKey = 'test';

            let sftpService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            console.log("instance obtained", sftpService);

            console.log("downloaded file from ..", fileRequest.remotePath);
            const res = await (await sftpService).get(fileRequest);
            return res;
        }
        catch (e) {
            console.log("some error occured while connecting to sftp", e);
        }
    }

    async sftpPut(fileRequest: IFileRequest) {
        try {
            const key = FileServiceProviderEnum.SFTP;
            const instanceKey = 'test';

            let sftpService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            console.log("instance obtained", sftpService);
            console.log("put request obtained is .. ",fileRequest);
            /** for upload */
            const res = await (await sftpService).putSync(fileRequest);
            return res;
        }
        catch (e) {
            console.log("some error occured while connecting to sftp", e);
        }
    }


    async sftpCopy(fileRequest: ICopyFileRequest) {
        try {
            const key = FileServiceProviderEnum.SFTP;
            const instanceKey = 'test';
            let sftpService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            console.log("instance obtained", sftpService);
            const res = await (await sftpService).copy(fileRequest);
            return res;
        }
        catch (e) {
            console.log("some error occured while connecting to sftp", e);
        }
    }

    async sftpDelete(fileRequest: IFileRequest) {
        try {
            const key = FileServiceProviderEnum.SFTP;
            const instanceKey = 'test';

            let sftpService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            console.log("instance obtained", sftpService);
            const res = (await sftpService).delete(fileRequest);
            return res;
        }
        catch (e) {
            console.log("some error occured while connecting to sftp", e);
        }
    }


}