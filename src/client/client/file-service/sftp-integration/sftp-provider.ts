import configs from "./config/sftpconfig.json";
import { IFileRequest, ICopyFileRequest } from "../file-request";
import { IFileService } from "../file-service";
import * as fs from 'fs';
import { CommonMethods } from "../common-methods";

/**
 * this class will implement the methods of IFileService which will be used
 * for sftp files upload, donload, copy as well as delete
 * @author Supriya
 */
export class SFTPProvider implements IFileService {
    private key: String;
    private static instance: SFTPProvider;
    constructor(key: String) {
        this.key = key;
    }

    private static fileServiceMap: Map<String, any> = new Map<String, String>();


    private static getFileInstance(key: String): SFTPProvider {
        console.log("inside get file instance");
        if (!SFTPProvider.instance) {
            SFTPProvider.instance = new SFTPProvider(key);
        }
        return SFTPProvider.instance;
    }

    static getInstance(key: String): SFTPProvider {
        console.log("inside get instance");
        if (!this.fileServiceMap.get(key)) {
            this.fileServiceMap.set(key, this.init(key));
            console.log("file service map is >> ", this.fileServiceMap);
        }
        return this.fileServiceMap.get(key);
    }

    private static init(key: String) {
        console.log("creating new instance for key", key);
        return SFTPProvider.getFileInstance(key);
    }

    /**
     * method to upload the file on SFTP
     * 
     * @param file 
     */
    async putSync(file: IFileRequest) {
        let Client = require('ssh2-sftp-client');
        let sftp = new Client();

        const config = {
            host: configs.SFTP_CONFIGURATION.host,
            port: configs.SFTP_CONFIGURATION.port,
            username: configs.SFTP_CONFIGURATION.username,
            password: configs.SFTP_CONFIGURATION.password
        };

        /** upload the file on sftp */
        if (file.buffer != null || file.base64 != null) {
            try {
                if (file.base64 != null) {
                    let data = file.base64;
                    let buffer = Buffer.from(data, 'base64');
                    await sftp.connect(config);
                    const res = await sftp.put(buffer, file.remotePath);
                    console.log("put response .. ", res);
                    await sftp.end();
                    return res;
                }
                else if (file.buffer != null) {
                    await sftp.connect(config);
                    const res = await sftp.put(file.buffer, file.remotePath);
                    console.log("put response .. ", res);
                    await sftp.end();
                    return res;
                }
            }
            catch (err) {
                console.log(err.message);
                return err;
            }
        }
        else if (file.remotePath != null && file.localPath != null) {
            try {
                await sftp.connect(config);
                const res = await sftp.fastPut(file.localPath, file.remotePath);
                console.log("put response .. ", res);
                await sftp.end();
                return res;
            }
            catch (err) {
                console.error(err.message);
                return err;
            }
        }
    }

    /**
     * method to download the file from SFTP
     * 
     * @param file 
     */
    async get(file: IFileRequest): Promise<any> {
        let Client = require('ssh2-sftp-client');
        let sftp = new Client();

        const config = {
            host: configs.SFTP_CONFIGURATION.host,
            port: configs.SFTP_CONFIGURATION.port,
            username: configs.SFTP_CONFIGURATION.username,
            password: configs.SFTP_CONFIGURATION.password
        };

        /** download the file from sftp */
        if (file.buffer === undefined && file.localPath == null) {
            console.log("inside buffer processing.......");
            try {
                await sftp.connect(config);
                const res = await sftp.get(file.remotePath, file.buffer);
                console.log("get response .. ", res);
                await sftp.end();
                return res;
            }
            catch (err) {
                console.error(err.message);
                return err;
            }
        }
        else if (file.remotePath != null && file.localPath != null) {
            try {
                await sftp.connect(config);
                const res = await sftp.fastGet(file.remotePath, file.localPath);
                console.log("get response .. ", res);
                await sftp.end();
                return res;
            }
            catch (err) {
                console.error(err.message);
                return err;
            }
        }
    }

    /**
     * method to copy file from one sftp location to another
     * 
     * @param file 
     */
    async copy(copyRequest: ICopyFileRequest) {
        /** prepare request for get and put of type IFileRequest */
        console.log("copy request .. ", copyRequest);
        let commonMethods = new CommonMethods();
        let tmpPath: string;
        tmpPath = await commonMethods.getTempFilePath();
        const file: IFileRequest = {
            localPath: tmpPath,
            remotePath: copyRequest.source
        }
        console.log("file request .. ", file);
        /** first download to tmp location then upload */
        console.log("downloading file to copy....", file);
        await this.get(file);
        file.localPath = tmpPath;
        file.remotePath = copyRequest.target;
        console.log("uploading file to copy....", file);
        const res = await this.putSync(file);
        console.log("deleting file from local location .. ", file.localPath);
        /** remove the file from tmp location after processing */
        fs.unlinkSync(tmpPath);
        return res;
    }

    /**
     * method to delete file from sftp
     * @param file 
     */
    async delete(file: IFileRequest): Promise<any> {
        let Client = require('ssh2-sftp-client');
        let sftp = new Client();

        const config = {
            host: configs.SFTP_CONFIGURATION.host,
            port: configs.SFTP_CONFIGURATION.port,
            username: configs.SFTP_CONFIGURATION.username,
            password: configs.SFTP_CONFIGURATION.password
        };
        try {
            await sftp.connect(config);
            const res = await sftp.delete(file.remotePath);
            console.log("delete response .. ", res);
            await sftp.end();
            return res;
        }
        catch (err) {
            console.log(err.message);
            return err;
        }
    }


    put(file: IFileRequest, cb: Function) {
        throw new Error("Method not implemented.");
    }

    /*private async createSftpConnection() {
        let Client = require('ssh2-sftp-client');
        let sftp = new Client();

        const config = {
            host: configs.SFTP_CONFIGURATION.host,
            port: configs.SFTP_CONFIGURATION.port,
            username: configs.SFTP_CONFIGURATION.username,
            password: configs.SFTP_CONFIGURATION.password
        };
        console.log("configs ... ", config);
        const conn = await sftp.connect(config);
        await sftp.cwd();
        await sftp.end();
        return conn;
    }*/
}