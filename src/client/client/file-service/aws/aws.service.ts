import * as AWS from 'aws-sdk';
import * as fs from 'fs';
import * as configs from '../aws/config/aws-data-config.json';
import { IFileService } from '../file-service';
import { IFileRequest, ICopyFileRequest } from '../file-request';
import { CommonMethods } from '../common-methods';

/**
 * @author siddharthsharma
 *
 */
export class AwsServiceProvider implements IFileService {
  private s3: AWS.S3;
  private static instanceMap = new Map();

  private constructor(
    private readonly S3_BUCKET_NAME,
    private readonly ACCESS_KEY_ID,
    private readonly SECRET_ACCESS_KEY,
  ) {
    this.S3_BUCKET_NAME = S3_BUCKET_NAME;
    this.ACCESS_KEY_ID = ACCESS_KEY_ID;
    this.SECRET_ACCESS_KEY = SECRET_ACCESS_KEY;
  }

  /**
   * The method deletes file from  S3
   * @param IFileRequest
   *  name?: string; => Name of file to be deleted
   *  localPath?: string;
   *  remotePath?: string; => Remote location of the file in the bucket
   *
   * @output Promise of AWS S3 response
   *
   */
  async delete(fileRequest: IFileRequest) {
    console.log('delete on request: ', fileRequest);
    return this.s3
      .deleteObject({
        Bucket: this.S3_BUCKET_NAME,
        Key: fileRequest.remotePath + fileRequest.name,
      })
      .promise();
  }

  /**
   * The method puts file from  S3 with callback
   * @param fileRequest: IFileRequest
   *  name?: string; => Desired Name of the file desired_test.txt
   *  localPath?: string; => Complete path to file test/test.txt
   *  remotePath?: string; => remote location in configured bucket for the instance
   *
   * @output Promise of AWS S3 response
   *
   */
  async putSync(fileRequest: IFileRequest) {
    console.log('putSync on request: ', fileRequest);
    let fileData = null;
    let filePath: string;
    if (fileRequest.buffer) {
      fileData = fileRequest.buffer;
    }
    if (fileRequest.base64) {
      let data = fileRequest.base64;
      let buff = Buffer.from(data, 'base64');
      fileData = buff;
      console.log("file data in putSync .. ",fileData);
    }
    else {
      fileData = fs.readFileSync(fileRequest.localPath);
    }
    if (!fileData) {
      return null;
    }
    const params = {
      Bucket: this.S3_BUCKET_NAME,
      Key: fileRequest.remotePath + fileRequest.name,
      Body: fileData,
    };
    const res= this.s3.putObject(params).promise();
    return res;
  }

  /**
   * Initializes the S3 instance for the configuration in constructor.
   * @output Instance of the S3-Provider
   */
  private async init(): Promise<AwsServiceProvider> {
    console.log('Initializing S3');
    this.s3 = await new AWS.S3({
      accessKeyId: this.ACCESS_KEY_ID,
      secretAccessKey: this.SECRET_ACCESS_KEY,
    });
    return this;
  }

  /**
   * @param key
   * Maintains a cache of multiple instances of S3 provider
   * for different connections.
   * @output instance of S3 provider.
   */
  public static async getInstance(key) {
    if (!this.instanceMap.get(key)) {
      const config = configs.SUBSCRIBER_1;
      this.instanceMap.set(
        key,
        await new AwsServiceProvider(
          config.bucketId,
          config.accessKeyId,
          config.secretAccessKey,
        ).init(),
      );
    }
    return this.instanceMap.get(key);
  }

  /**
   * @param fileRequest: IFileRequest
   *  name?: string; => Desired Name of the file desired_test.txt
   *  localPath?: string; => Complete path to file test/test.txt
   *  remotePath?: string; => remote location in configured bucket for the instance
   *
   * @param cb
   * Put file on S3 with callback.
   *
   */
  put(fileRequest: IFileRequest, cb: Function) {
    console.log('put on request: ', fileRequest);
    let params = null;
    fs.readFile(fileRequest.localPath, (e, d) => {
      if (e) throw e;
      params = {
        Bucket: this.S3_BUCKET_NAME,
        Key: fileRequest.remotePath + fileRequest.name,
        Body: d,
      };
      if (params) {
        this.s3
          .putObject(params)
          .promise()
          .then(
            data => {
              return cb(null, data);
            },
            err => {
              console.log(err);
              return cb(err, null);
            },
          );
      } else {
        return cb(null);
      }
    });
  }

  /**
   * @param fileRequest: IFileRequest
   *  name?: string; => Name of the file
   *  localPath?: string;
   *  remotePath?: string; => remote location in configured bucket for the instance
   *
   * Get file from S3 with callback.
   *
   */
  async get(fileRequest: IFileRequest) {
    console.log('get on request: ', fileRequest);
    const params = {
      Bucket: this.S3_BUCKET_NAME,
      Key: fileRequest.remotePath + fileRequest.name,
    };
    var putObjectPromise = this.s3.getObject(params).promise();
    return putObjectPromise;
  }

  /**
   * @param fileRequest: ICopyFileRequest
   *   sourceName?: string; => source file name to be copied
   *   source?: string; => source directory
   *   target?: string; => target directory
   *   targetName?: string => target file name
   * Copy file from S3 location to local localtion with callback.
   *
   */
  async copy(fileCopyRequest: ICopyFileRequest) {
    console.log('copy on request: ', fileCopyRequest);
    const params = {
      Bucket: this.S3_BUCKET_NAME,
      CopySource: this.S3_BUCKET_NAME + '/' + fileCopyRequest.source + fileCopyRequest.sourceName,
      Key: fileCopyRequest.target + fileCopyRequest.targetName,
    };
    var putObjectPromise = await this.s3.copyObject(params).promise();
    return putObjectPromise;
  }
}
