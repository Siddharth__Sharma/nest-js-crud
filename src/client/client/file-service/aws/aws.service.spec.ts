import { FileServiceProvider } from '../file.service-provider';
import { FileServiceProviderEnum } from '../file-service-provider.enum';
import { IFileRequest, ICopyFileRequest } from '../../file-service/file-request';
import { IFileService } from '../file-service';
import * as fs from 'fs';
describe('AWS Service Test', () => {
  let awsFileService: IFileService;
  const file1 = {
    name: 'file1.txt',
    path : '/tmp/file1.txt',
    data : 'This is file1 text.'
  }; 
  const file2 = {
    name: 'file2.txt',
    path : '/tmp/file2.txt',
    data : 'This is file2 text.'
  };
  const file3 = {
    name: 'file3.txt',
    path : '/tmp/file3.txt',
    data : 'This is file3 text.'
  };
  beforeAll(async () => {
     fs.writeFileSync(file1.path, file1.data);
     fs.writeFileSync(file2.path, file2.data);
     fs.writeFileSync(file3.path, file3.data);
    awsFileService = await FileServiceProvider.getInstance().getFileServiceProvider(
      FileServiceProviderEnum.AWS_S3,
      'SUBSCRIBER_1',
    );
  });
  it('Should upload File test1', async done => {
    const request: IFileRequest = {
      name: file1.name,
      localPath: file1.path,
      remotePath: 'file-upload-test/',
    };
    awsFileService.put(request, (error, data) => {
      expect(data).toStrictEqual({ ETag: expect.anything() });
      done();
    });
  });

  it('Should upload file in sync test2', async () => {
    const request: IFileRequest = {
      name: file2.name,
      localPath: file2.path,
      remotePath: 'file-upload-test/',
    };
    const data = await awsFileService.putSync(request);
    expect(data).toStrictEqual({ ETag: expect.anything() });
  });

  it('Should upload file in sync from buffer test2', async () => {
    const request: IFileRequest = {
      name: file3.name,
      localPath: file3.path,
      remotePath: 'file-upload-test/',
      buffer: fs.readFileSync(file3.path)
    };
    const data = await awsFileService.putSync(request);
    expect(data).toStrictEqual({ ETag: expect.anything() });
  });

  it('Should upload file in sync test2', async () => {
    const request: IFileRequest = {
      name: file2.name,
      localPath: file2.path,
      remotePath: 'file-upload-test/',
    };
    const data = await awsFileService.putSync(request);
    expect(data).toStrictEqual({ ETag: expect.anything() });
  });

  it('Should get file from s3', async () => {
    const request: IFileRequest = {
      name: 'test_2.txt',
      localPath: '/Users/siddharthsharma/Documents/',
      remotePath: 'file-upload-test/',
    };
    const file = await awsFileService.get(request);
    expect(file.ETag).toStrictEqual(expect.anything());
  });

  it('Should delete file from remote', async () => {
    const request: IFileRequest = {
      name: file1.name,
      remotePath: 'file-upload-test/',
    };
    const result = await awsFileService.delete(request);
    expect(result);
  });

  it('Should Copy file from source to target', async () => {
      const request: ICopyFileRequest = {
          sourceName: 'test_2.txt',
          source: 'file-upload-test/',
          target: 'file-upload-test-target/',
          targetName: 'test_2_copy.txt'
      }
      const result = await awsFileService.copy(request);
      expect(result);
  });
});
