import { FileServiceProviderEnum } from "../file-service-provider.enum";
import { IFileRequest, ICopyFileRequest } from "../file-request";
import { FileServiceProvider } from "../file.service-provider";

export class AWSTest {
    async s3Get(fileRequest: IFileRequest) {
        try {
            const key = FileServiceProviderEnum.AWS_S3;
            const instanceKey = 'SUBSCRIBER_1';

            let awsService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            return await (await awsService).get(fileRequest);
        }
        catch (e) {
            console.log("some error occured while connecting to s3", e);
        }
    }

    async s3PutSync(fileRequest: IFileRequest) {
        try {
            const key = FileServiceProviderEnum.AWS_S3;
            const instanceKey = 'SUBSCRIBER_1';

            let awsService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            return await (await awsService).putSync(fileRequest);
        }
        catch (e) {
            console.log("some error occured while connecting to s3", e);
        }
    }

    async s3Delete(fileRequest: IFileRequest) {
        try {
            const key = FileServiceProviderEnum.AWS_S3;
            const instanceKey = 'SUBSCRIBER_1';

            let awsService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            return await (await awsService).delete(fileRequest);
        }
        catch (e) {
            console.log("some error occured while connecting to s3", e);
        }
    }

    async s3Copy(copyFileRequest: ICopyFileRequest) {
        try {
            const key = FileServiceProviderEnum.AWS_S3;
            const instanceKey = 'SUBSCRIBER_1';

            let awsService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            console.log("aws service", awsService);
            return (await awsService).copy(copyFileRequest);
        }
        catch (e) {
            console.log("some error occured while connecting to s3", e);
        }
    }

    async s3Put(fileRequest: IFileRequest) {
        try {
            const key = FileServiceProviderEnum.AWS_S3;
            const instanceKey = 'SUBSCRIBER_1';

            let awsService = FileServiceProvider.getInstance().getFileServiceProvider(key, instanceKey);
            return await (await awsService).put(fileRequest, ()=>{
                console.log("put function with callback");
            });
        }
        catch (e) {
            console.log("some error occured while connecting to s3", e);
        }
    } 
    
}