import * as base64 from 'file-base64';
import * as fs from 'fs';

export class CommonMethods {

    async getFileFromBase64(base64Content: string) {
        let filePath = CommonConstants.TMP_PATH + Math.floor(Date.now() / 1000) + CommonConstants.TMP_FILE_EXTENSION;
        console.log("created file from base 64 on local .. ");
        await base64.decode(base64Content, filePath, function (err, output) { console.log('success'); });
        /** store the file at temp location */
        await fs.createReadStream(filePath);
        console.log("file path returned is .. ",filePath);
        return filePath;
    }

    async getTempFilePath(){
        let filePath = CommonConstants.TMP_PATH + Math.floor(Date.now() / 1000) + CommonConstants.TMP_FILE_EXTENSION;
        return filePath;
    }
}



export class CommonConstants{
    public static TMP_PATH = "/tmp/";
    public static TMP_FILE_EXTENSION = ".tmp";
}