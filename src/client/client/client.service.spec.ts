import { Test, TestingModule } from '@nestjs/testing';
import { ClientService } from './client.service';
import { FileServiceProviderEnum } from './file-service/sftp-integration/file-service-provider.enum';
import { FileServiceProvider } from './file-service/sftp-integration/file-service-provider';
import { IFileService } from './file-service/sftp-integration/ifile-service';
import { IFileRequest } from './file-service/sftp-integration/ifile-request';

describe('ClientService', () => {
  let sftpService: IFileService;
  beforeAll(async () => {
    sftpService = await FileServiceProvider.getInstance().getFileServiceProvider(
      FileServiceProviderEnum.SFTP,
      'SUBSCRIBER_1',
    );
  });
  

  it('should be defined', async() => {
    var fs = require('fs');
    let remotePath = '/firefly/PNX/document_product_update/PNX_Document_Update_D20200612T0552360068.csv';
    let dst = fs.createWriteStream('/Users/supriya/Downloads/sftp-testing/sftp_test_stream_8.csv');
    let fileRequest : IFileRequest;
    fileRequest.localFileStream = dst;
    fileRequest.remoteFileStream = remotePath;
    await sftpService.get(fileRequest);
    console.log("logger after async");
  });
});
