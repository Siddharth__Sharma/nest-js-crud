import * as mongoose from 'mongoose';

export const ClientSchema = new mongoose.Schema({
  id: Number,
  firstName: String,
  lastName: String,
  email: String,
  phoneNumber: String,
  city: String,
  country: String,
},{
  versionKey:false
});