import { Model, Document } from "mongoose";
import { IsNotEmpty } from "class-validator";

export interface Client extends Document{
  id: Number,
  firstName: String ,
  lastName: String ,
  email: String ,
  phoneNumber: String ,
  city: String ,
  country: String,
}