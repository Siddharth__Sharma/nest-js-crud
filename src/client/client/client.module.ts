import { Module } from '@nestjs/common';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientSchema } from './client.schema';
import { TwilioTest } from './twilio-integration/twilio-test';
import { SFTPTest } from './file-service/sftp-integration/sftp-test';
import { AwsServiceProvider } from './file-service/aws/aws.service';
import { SESEmailProvider } from './aws-ses-email/ses-email/email-provider/ses-email-provider';
import { SMTPEmailProvider } from './smtp-email-integration/smtp-email/email-provider/smtp-email-provider';
import { AWSTest } from './file-service/aws/aws-test';
import { SESService } from './aws-ses-email/ses.service';
import { SMTPService } from './smtp-email-integration/smtp.service';


@Module({
    imports : [MongooseModule.forFeature([{ name: 'Client', schema: ClientSchema, collection : 'Clients' }])],
    controllers :[ClientController],
    providers : [ClientService, TwilioTest, SFTPTest, AWSTest, SESService, SMTPService, SESEmailProvider,SMTPEmailProvider],
})
export class ClientModule {}
