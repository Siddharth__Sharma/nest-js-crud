import { Module, ValidationPipe } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule, InjectConnection } from "@nestjs/mongoose";
import { Connection } from "mongoose";
import { ClientModule } from './client/client/client.module';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017/localhost'), ClientModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  // constructor(@InjectConnection() private readonly connection: Connection) {}
}
